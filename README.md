lol_timer
=========

League of Legends jungle timer.

Requirements: python2 and pygame

This is a timing script for the game League of Legends. After killing a jungle buff enter the corresponding command into console and 
the script will set a timer letting you know X seconds before the buff re-spawns.

I have no idea where I got the audio from originally. I think it was some AT&T site that was demonstrating their new automated voice system.

#!/usr/bin/env python

from threading import Timer
import time, sys, pygame
pygame.mixer.init(16000, -16, 2, 512)

######################################### GLOBALS #####################################################
DLY = 10
REDBLUSP = 115 #1.55 Blue Golum and Red Lizard (Initial Spawn)
REDBLURS = 300-DLY #5.00 Blue / Red (Respawn Time)
BARONRS = 420-DLY #7.00 Baron Nashor (Respawn Time)
BARONSP = 900-DLY #15.00 Baron (Initial Spawn)
DRAGONRS = 360-DLY #6.00 Dragon (Respawn Time)
DRAGONSP = 150 #2.30 Dragon (Initial Spawn)
NPCTIME = 100 #1.40 Golums, Wraiths, Wolves (Initial Spawn & Respawn Time)

GAMETIME = 0.0

eblu = pygame.mixer.Sound("./audio/enemy-blue-spawned.wav")
eblut = None
ablu = pygame.mixer.Sound("./audio/our-blue-spawned.wav")
ablut = None
ered = pygame.mixer.Sound("./audio/enemy-red-spawned.wav")
eredt = None
ared = pygame.mixer.Sound("./audio/our-red-spawned.wav")
aredt = None
baro = pygame.mixer.Sound("./audio/baron-spawned.wav")
barot = None
drag = pygame.mixer.Sound("./audio/dragon-spawned.wav")
dragt = None
#######################################################################################################



def helpTxt():
	print "e is enemy, a is allied.\nr is red, b is blue.\nbar is baron, dra is dragon\nexit to exit"
helpTxt()

def cleanExit():
	if eblut: eblut.cancel()
	if ablut: ablut.cancel()
	if eredt: eredt.cancel()
	if aredt: aredt.cancel()
	if barot: barot.cancel()
	if dragt: dragt.cancel()
	print("Goodbye :D")
	sys.exit()	

def sndBlu(e):
	if e:
		eblu.play()
		print("Enemy Blue Respawned\n")
	else:
		ablu.play()
		print("Allied Blue Respawned\n")
def sndRed(e):
	if e:
		ered.play()
		print("Enemy Red Respawned\n")
	else:
		ared.play()
		print("Allied Red Respawned\n")
def sndBar():
	baro.play()
	print("Baron Respawned\n")
def sndDra():
	drag.play()
	print("Dragon Respawned\n")

barot = Timer(BARONSP, sndBar)
barot.start()
try:
	while(True):
################################### Base Code Below ###########################################
		x = raw_input("#")

		if x == "eb":
			if eblut: eblut.cancel()
			eblut = Timer(REDBLURS, sndBlu, [1])
			eblut.start()
		elif x == "ab":
			if ablut: ablut.cancel()
			ablut = Timer(REDBLURS, sndBlu, [0])
			ablut.start()
		elif x == "er":
			if eredt: eredt.cancel()
			eredt = Timer(REDBLURS, sndRed, [1])
			eredt.start()
		elif x == "ar":
			if aredt: aredt.cancel()
			aredt = Timer(REDBLURS, sndRed, [0])
			aredt.start()
		elif x == "bar":
			if barot: barot.cancel()
			barot = Timer(BARONRS, sndBar)
			barot.start()
		elif x == "dra":
			if dragt: dragt.cancel()
			dragt = Timer(DRAGONRS, sndDra)
			dragt.start()
		elif x == "h" or x == "?": helpTxt()
		elif x == "exit": cleanExit()
		else: pass #helpTxt()
###############################################################################################
except KeyboardInterrupt:
	print("Goodbye :D\n")
	sys.exit()
